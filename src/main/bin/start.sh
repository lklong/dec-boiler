#!/bin/bash

unset PROJECT_HOME
export PROJECT_HOME="$(cd "`dirname "$0"`"/..; pwd)"
. $PROJECT_HOME/bin/load-env.sh

ps -ef | grep $CLASSNAME | grep -v grep >/dev/null 2>&1 
if [ $? -eq 0 ];then
  echo "dec-dg-datainterface has been started already!"
  exit 1;
fi

echo 'dec-dg-datainterface is starting ...'

nohup java -DPROJECT_HOME=$PROJECT_HOME \
           -Dlogging.config=$PROJECT_HOME/conf/log4j2.xml \
           -cp $CLASSPATH $CLASSNAME  --spring.config.location=$PROJECT_HOME/conf/application.properties >/dev/null 2>&1 &

echo 'Started!!!'