package com.calabar.entity;

/**
 * <p/>
 * <li>Description: 插入数据实体类</li>
 * <li>@author: zijian.wu</li>
 * <li>Date: 2018/4/20 13:33</li>
 */

public class DataPutEntity {
    /**
     * 测点标准编码
     */
    private String stdCode;

    /**
     * 电厂编码
     */
    private String pltCode;

    /**
     * 机组编码
     */
    private String setCode;

    /**
     * 时间戳
     */
    private Long timestamp;

    /**
     * 值
     */
    private Float value;

    /**
     * 质量标签
     */
    private Integer quality;

    /**
     * 无参构造
     */
    public DataPutEntity() {
    }

    /**
     * 构造器
     *
     * @param stdCode   测点标准编码
     * @param pltCode   电厂编码
     * @param setCode   机组编码
     * @param timestamp 时间戳
     * @param value     值
     * @param quality   质量标签
     */
    public DataPutEntity(String stdCode, String pltCode, String setCode, Long timestamp, Float value, Integer quality) {
        this.stdCode = stdCode;
        this.pltCode = pltCode;
        this.setCode = setCode;
        this.timestamp = timestamp;
        this.value = value;
        this.quality = quality;
    }

    /**
     * 获取测点标准编码
     *
     * @return stdCode
     */
    public String getStdCode() {
        return stdCode;
    }

    /**
     * 设置测点标准编码
     *
     * @param stdCode stdCode
     */
    public void setStdCode(String stdCode) {
        this.stdCode = stdCode;
    }

    /**
     * 获取电厂编码
     *
     * @return stdCode
     */
    public String getPltCode() {
        return pltCode;
    }

    /**
     * 设置电厂编码
     *
     * @param pltCode pltCode
     */
    public void setPltCode(String pltCode) {
        this.pltCode = pltCode;
    }

    /**
     * 获取机组编码
     *
     * @return setCode
     */
    public String getSetCode() {
        return setCode;
    }

    /**
     * 设置机组编码
     *
     * @param setCode setCode
     */
    public void setSetCode(String setCode) {
        this.setCode = setCode;
    }

    /**
     * 获取时间戳
     *
     * @return timestamp
     */
    public Long getTimestamp() {
        return timestamp;
    }

    /**
     * 设置机组编码时间戳
     *
     * @param timestamp timestamp
     */
    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * 获取值
     *
     * @return timestamp
     */
    public Float getValue() {
        return value;
    }

    /**
     * 设置值
     *
     * @param value value
     */
    public void setValue(Float value) {
        this.value = value;
    }

    /**
     * 质量标签
     *
     * @return quality
     */
    public Integer getQuality() {
        return quality;
    }

    /**
     * 设置质量标签
     *
     * @param quality quality
     */
    public void setQuality(Integer quality) {
        this.quality = quality;
    }
}
