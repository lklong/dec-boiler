package com.calabar.entity;

import java.util.Map;

/**
 * <p/>
 * <li>Description: 时序数据库实体类</li>
 * <li>@author: zijian.wu</li>
 * <li>Date: 2018/4/20 13:54</li>
 */

public class OpenTsdbDataEntity {

    /**
     * 指标
     */
    private String metric;

    /**
     * 标签
     */
    private Map<String, String> tags;

    /**
     * 时间戳
     */
    private Long timestamp;

    /**
     * 值
     */
    private float value;

    /**
     * 无参构造
     */
    public OpenTsdbDataEntity() {
    }

    /**
     * 构造器
     *
     * @param metric    指标
     * @param tags      标签
     * @param timestamp 时间戳
     * @param value     值
     */
    public OpenTsdbDataEntity(String metric, Map<String, String> tags, Long timestamp, float value) {
        this.metric = metric;
        this.tags = tags;
        this.timestamp = timestamp;
        this.value = value;
    }

    /**
     * 获取指标
     *
     * @return metric
     */
    public String getMetric() {
        return metric;
    }

    /**
     * 设置指标
     *
     * @param metric metric
     */
    public void setMetric(String metric) {
        this.metric = metric;
    }

    /**
     * 获取标签
     *
     * @return tags
     */
    public Map<String, String> getTags() {
        return tags;
    }

    /**
     * 设置标签
     *
     * @param tags tags
     */
    public void setTags(Map<String, String> tags) {
        this.tags = tags;
    }

    /**
     * 获取时间戳
     *
     * @return timestamp
     */
    public Long getTimestamp() {
        return timestamp;
    }

    /**
     * 设置时间戳
     *
     * @param timestamp timestamp
     */
    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * 获取值
     *
     * @return value
     */
    public float getValue() {
        return value;
    }

    /**
     * 设置值
     *
     * @param value value
     */
    public void setValue(float value) {
        this.value = value;
    }
}
