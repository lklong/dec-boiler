package com.calabar.entity;

import java.util.List;

/**
 * <p/>
 * <li>Description: 实时查询实体类</li>
 * <li>@author: zijian.wu</li>
 * <li>Date: 2018/4/12 9:49</li>
 */

public class RealTimeQueriesEntity {
    /**
     * 测点标准编码
     */
    private List<String> stdCodes;

    /**
     * 电厂编码
     */
    private String pltCode;

    /**
     * 机组编码
     */
    private String setCode;

    /**
     * 无参构造
     */
    public RealTimeQueriesEntity() {
    }

    /**
     * 构造器
     *
     * @param stdCodes 测点标准编码
     * @param pltCode  电厂编码
     */
    public RealTimeQueriesEntity(List<String> stdCodes, String pltCode) {
        this.stdCodes = stdCodes;
        this.pltCode = pltCode;
    }

    /**
     * 获取测点标准编码
     *
     * @return stdCodes
     */
    public List<String> getStdCodes() {
        return stdCodes;
    }

    /**
     * 设置测点标准编码
     *
     * @param stdCodes stdCodes
     */
    public void setStdCodes(List<String> stdCodes) {
        this.stdCodes = stdCodes;
    }

    /**
     * 获取电厂编码
     *
     * @return pltCode
     */
    public String getPltCode() {
        return pltCode;
    }

    /**
     * 设置电厂编码
     *
     * @param pltCode pltCode
     */
    public void setPltCode(String pltCode) {
        this.pltCode = pltCode;
    }

    /**
     * 获取机组编码
     *
     * @return setCode
     */
    public String getSetCode() {
        return setCode;
    }

    /**
     * 设置机组编码
     *
     * @param setCode setCode
     */
    public void setSetCode(String setCode) {
        this.setCode = setCode;
    }
}
