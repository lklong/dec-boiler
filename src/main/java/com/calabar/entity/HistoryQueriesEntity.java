package com.calabar.entity;

import java.util.List;

/**
 * <p/>
 * <li>Description: 历史查询实体类</li>
 * <li>@author: zijian.wu</li>
 * <li>Date: 2018/4/12 9:44</li>
 */

public class HistoryQueriesEntity {
    /**
     * 测点标准编码
     */
    private List<String> stdCodes;

    /**
     * 电厂编码
     */
    private String pltCode;

    /**
     * 机组编码
     */
    private String setCode;

    /**
     * 起始时间
     */
    private Long startTime;

    /**
     * 结束时间
     */
    private Long endTime;
    /**
     * 插值填充方法
     */
    private Integer filling;

    /**
     * 采样间隔
     */
    private Integer sampleInterval;

    /**
     * 采样函数
     */
    private Integer sampleFunc;

    /**
     * 无参构造
     */
    public HistoryQueriesEntity() {
    }

    /**
     * 构造器
     *
     * @param stdCodes       测点标准编码
     * @param pltCode        电厂编码
     * @param startTime      起始时间
     * @param endTime        结束时间
     * @param filling        插值填充方法
     * @param sampleInterval 采样间隔
     * @param sampleFunc     采样函数
     */
    public HistoryQueriesEntity(List<String> stdCodes, String pltCode, String setCode, Long startTime, Long endTime, Integer filling, Integer sampleInterval, Integer sampleFunc) {
        this.stdCodes = stdCodes;
        this.pltCode = pltCode;
        this.setCode = setCode;
        this.startTime = startTime;
        this.endTime = endTime;
        this.filling = filling;
        this.sampleInterval = sampleInterval;
        this.sampleFunc = sampleFunc;
    }

    /**
     * 获取测点标准编码
     *
     * @return stdCodes
     */
    public List<String> getStdCodes() {
        return stdCodes;
    }

    /**
     * 设置测点标准编码
     *
     * @param stdCodes stdCodes
     */
    public void setStdCodes(List<String> stdCodes) {
        this.stdCodes = stdCodes;
    }

    /**
     * 获取电厂编码
     *
     * @return pltCode
     */
    public String getPltCode() {
        return pltCode;
    }

    /**
     * 设置电厂编码
     *
     * @param pltCode pltCode
     */
    public void setPltCode(String pltCode) {
        this.pltCode = pltCode;
    }

    /**
     * 获取机组编码
     *
     * @return setCode
     */
    public String getSetCode() {
        return setCode;
    }

    /**
     * 设置机组编码
     *
     * @param setCode setCode
     */
    public void setSetCode(String setCode) {
        this.setCode = setCode;
    }

    /**
     * 获取起始时间
     *
     * @return startTime
     */
    public Long getStartTime() {
        return startTime;
    }

    /**
     * 设置起始时间
     *
     * @param startTime startTime
     */
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    /**
     * 获取结束时间
     *
     * @return endTime
     */
    public Long getEndTime() {
        return endTime;
    }

    /**
     * 设置结束时间
     *
     * @param endTime endTime
     */
    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    /**
     * 获取插值填充方法
     *
     * @return filling
     */
    public Integer getFilling() {
        return filling;
    }

    /**
     * 设置插值填充方法
     *
     * @param filling filling
     */
    public void setFilling(Integer filling) {
        this.filling = filling;
    }




    /**

     * 获取采样间隔
     *
     * @return filling
     */
    public Integer getSampleInterval() {
        return sampleInterval;
    }

    /**
     * 设置采样间隔
     *
     * @param sampleInterval sampleInterval
     */
    public void setSampleInterval(Integer sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    /**
     * 获取采样函数
     *
     * @return sampleFunc
     */
    public Integer getSampleFunc() {
        return sampleFunc;
    }

    /**
     * 设置采样函数
     *
     * @param sampleFunc sampleFunc
     */
    public void setSampleFunc(Integer sampleFunc) {
        this.sampleFunc = sampleFunc;
    }
}
