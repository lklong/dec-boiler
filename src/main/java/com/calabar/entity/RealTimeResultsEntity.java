package com.calabar.entity;


/**
 * <p/>
 * <li>Description: 实时查询结果实体类</li>
 * <li>@author: zijian.wu</li>
 * <li>Date: 2018/4/8 9:17</li>
 */

public class RealTimeResultsEntity {
    /**
     * 测点标准编码
     */
    private String stdCode;

    /**
     * 时间戳
     */
    private long timestamp;

    /**
     * 测点值
     */
    private float value;

    /**
     * 数据质量
     */
    private float quality;

    /**
     * 无参构造
     */
    public RealTimeResultsEntity() {

    }

    /**
     * 构造器
     *
     * @param stdCode   测点标准编码
     * @param timestamp 时间戳
     * @param value     测点值
     * @param quality   测点质量标签
     */
    public RealTimeResultsEntity(String stdCode, long timestamp, float value, float quality) {
        this.stdCode = stdCode;
        this.timestamp = timestamp;
        this.value = value;
        this.quality = quality;
    }

    /**
     * 获取测点标准编码
     *
     * @return stdCodes
     */
    public String getStdCode() {
        return stdCode;
    }

    /**
     * 设置测点标准编码
     *
     * @param stdCode stdCode
     */
    public void setStdCode(String stdCode) {
        this.stdCode = stdCode;
    }

    /**
     * 获取时间戳
     *
     * @return pltCode
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * 设置时间戳
     *
     * @param timestamp timestamp
     */
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * 获取测点值
     *
     * @return value
     */
    public float getValue() {
        return value;
    }

    /**
     * 设置测点值
     *
     * @param value value
     */
    public void setValue(float value) {
        this.value = value;
    }

    /**
     * 获取测点质量标签
     *
     * @return value
     */
    public float getQuality() {
        return quality;
    }

    /**
     * 设置测点质量标签
     *
     * @param quality quality
     */
    public void setQuality(float quality) {
        this.quality = quality;
    }
}
