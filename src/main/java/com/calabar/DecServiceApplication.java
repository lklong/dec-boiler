package com.calabar;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p/>
 * <li>Description: 主函数</li>
 * <li>@author: zijian.wu</li>
 * <li>Date: 2018/4/8 16:41</li>
 */
@SpringBootApplication
public class DecServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(DecServiceApplication.class, args);
    }
}
