package com.calabar.enum_package;

/**
 * <p/>
 * <li>Description: 采样函数枚举</li>
 * <li>@author: zijian.wu</li>
 * <li>Date: 2018/4/3 10:25</li>
 */

public enum SampleFunc {
    /**
     * 平均值
     */
    AVG("avg"),

    /**
     * 最小值
     */
    MIN("min"),

    /**
     * 最大值
     */
    MAX("max"),

    /**
     * 首值
     */
    OPEN("first"),

    /**
     * 尾值
     */
    CLOSE("last");

    /**
     * 采样函数func
     */
    private String func;

    /**
     * 构造器
     *
     * @param func 采样函数func
     */
    SampleFunc(String func) {
        this.func = func;
    }

    /**
     * 获取采样函数
     *
     * @return func
     */
    public String getFunc() {
        return func;
    }

    /**
     * 设置采样函数
     *
     * @param func 采样函数func
     */
    public void setFunc(String func) {
        this.func = func;
    }
}
