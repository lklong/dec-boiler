package com.calabar.enum_package;

/**
 * <p/>
 * <li>Description: 插值填充枚举</li>
 * <li>@author: zijian.wu</li>
 * <li>Date: 2018/4/3 10:25</li>
 */

public enum Filling {
    /**
     * 不进行插值填充
     */
    NONE(0),

    /**
     * 线性插值填充
     */
    LERP(1),

    /**
     * 取前插值填充
     */
    PRE(2);


    /**
     * 对应编码
     */
    private int code;

    /**
     * 构造器
     *
     * @param code 对应编码
     */
    Filling(int code) {
        this.code = code;
    }

    /**
     * 获取对应编码
     *
     * @return code
     */
    public int getCode() {
        return code;
    }

    /**
     * 设置对应编码
     *
     * @param code 对应编码
     */
    public void setCode(int code) {
        this.code = code;
    }
}
