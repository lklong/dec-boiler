package com.calabar.controller;

import com.alibaba.fastjson.JSONArray;
import com.calabar.entity.DataPutEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.List;

/**
 * <p/>
 * <li>Description: 插入数据参数验证</li>
 * <li>@author: zijian.wu</li>
 * <li>Date: 2018/4/20 14:55</li>
 */

public class PutParamsValidate {
    /**
     * 日志记录
     */
    private static final Logger LOGGER = LogManager.getLogger(QueryParamsValidate.class);

    public static void putParamsValidate(String json) throws Exception {
        List<DataPutEntity> dataList = null;
        //JSON格式验证
        try {
            dataList = JSONArray.parseArray(json, DataPutEntity.class);
        } catch (Exception e) {
            LOGGER.error("Json can not be parsed");
            throw new Exception("Json can not be parsed");
        }

        //为空验证
        if (dataList != null && dataList.size() > 0) {
            for (DataPutEntity data : dataList) {
                if (data.getStdCode() == null
                        || data.getPltCode() == null
                        || data.getSetCode() == null
                        || data.getTimestamp() == null
                        || data.getValue() == null
                        || data.getQuality() == null) {
                    LOGGER.error("Params can not be null");
                    throw new Exception("Params can not be null");
                }
            }
        }
    }
}
