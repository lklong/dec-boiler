package com.calabar.controller;

import com.alibaba.fastjson.JSON;
import com.calabar.entity.HistoryQueriesEntity;
import com.calabar.entity.RealTimeQueriesEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <p/>
 * <li>Description: 查询json字符串串参数验证</li>
 * <li>@author: zijian.wu</li>
 * <li>Date: 2018/4/11 10:18</li>
 */

public class QueryParamsValidate {
    /**
     * 日志记录
     */
    private static final Logger LOGGER = LogManager.getLogger(QueryParamsValidate.class);
    /**
     * 插值枚举范围
     */
    private static final int[] FILLING_RANGE = {0, 2};
    /**
     * 采样函数枚举范围
     */
    private static final int[] SAMPLE_FUN_RANGE = {0, 4};
    /**
     * 采样时间间隔范围
     */
    private static final int INTERVAL_RANGE = 0;
    /**
     * 时间戳警告最大秒数
     */
    private static final int TIMESTAMP_ALARM = 3600;

    /**
     * 实时查询参数验证
     *
     * @param json 查询条件的json字符串
     * @throws Exception 查询参数异常
     */
    public static void realtimeParamsValidate(String json) throws Exception {
        RealTimeQueriesEntity realTimeQueriesEntity = null;
        //JSON格式验证
        try {
            realTimeQueriesEntity = JSON.parseObject(json, RealTimeQueriesEntity.class);
        } catch (Exception e) {
            LOGGER.error("Params error,json can not be parsed");
            throw new Exception("Params error,json can not be parsed");
        }
        //空值验证
        if (realTimeQueriesEntity.getStdCodes() == null
                || realTimeQueriesEntity.getPltCode() == null
                || realTimeQueriesEntity.getSetCode() == null) {
            LOGGER.error("Params error,json can not be parsed");
            throw new Exception("Params error,json can not be parsed");
        }
    }

    /**
     * 历史查询参数验证
     *
     * @param json 查询条件的json字符串
     * @throws Exception 查询参数异常
     */
    public static void historyQueriesValidate(String json) throws Exception {
        HistoryQueriesEntity historyQueriesEntity = null;
        //JSON格式验证
        try {
            historyQueriesEntity = JSON.parseObject(json, HistoryQueriesEntity.class);
        } catch (Exception e) {
            LOGGER.error("Params error,json can not be parsed");
            throw new Exception("Params error,json can not be parsed");
        }
        //空值验证
        if (historyQueriesEntity == null
                || historyQueriesEntity.getStdCodes() == null
                || historyQueriesEntity.getPltCode() == null
                || historyQueriesEntity.getSetCode() == null
                || historyQueriesEntity.getStartTime() == null
                || historyQueriesEntity.getEndTime() == null
                || historyQueriesEntity.getFilling() == null
                || historyQueriesEntity.getSampleInterval() == null
                || historyQueriesEntity.getSampleFunc() == null) {
            LOGGER.error("Params error,json can not be parsed");
            throw new Exception("Params error,json can not be parsed");
        }
        //起止时间验证
        if (historyQueriesEntity.getStartTime() > historyQueriesEntity.getEndTime()) {
            LOGGER.error("End time must be after start time!");
            throw new Exception("End time must be after start time!");
        }
        //插值枚举验证
        if (historyQueriesEntity.getFilling() > FILLING_RANGE[1] || historyQueriesEntity.getFilling() < FILLING_RANGE[0]) {
            LOGGER.error("Param of filling error");
            throw new Exception("Param of filling error");
        }
        //采样函数验证
        if (historyQueriesEntity.getSampleFunc() > SAMPLE_FUN_RANGE[1] || historyQueriesEntity.getSampleFunc() < SAMPLE_FUN_RANGE[0]) {
            LOGGER.error("Param of sampleFunc error");
            throw new Exception("Param of sampleFunc error");
        }
        //时间间隔验证
        if (historyQueriesEntity.getSampleInterval() < INTERVAL_RANGE || historyQueriesEntity.getSampleInterval() == INTERVAL_RANGE) {
            LOGGER.error("Param of sampleInterval must be positive");
            throw new Exception("Param of sampleInterval must be positive");
        }
        //时间戳验证
        if (historyQueriesEntity.getEndTime() < 0 || historyQueriesEntity.getStartTime() < 0) {
            LOGGER.error("startTime or endTime can not be negetive");
            throw new Exception("startTime or endTime can not be negetive");
        }
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date startDate = new Date(historyQueriesEntity.getStartTime());
            simpleDateFormat.format(startDate);
            Date endDate = new Date(historyQueriesEntity.getEndTime());
            simpleDateFormat.format(endDate);
            if (historyQueriesEntity.getEndTime() - historyQueriesEntity.getStartTime() > TIMESTAMP_ALARM) {
                LOGGER.warn("The interval between startTime and endTime may cause a large number of data");
            }
        } catch (Exception e) {
            LOGGER.error("startTime or endTime error");
            throw new Exception("startTime or endTime error");
        }
    }


}
