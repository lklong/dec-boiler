package com.calabar.controller;

import com.alibaba.fastjson.JSON;
import com.calabar.entity.HistoryQueriesEntity;
import com.calabar.entity.RealTimeQueriesEntity;
import com.calabar.enum_package.Filling;
import com.calabar.enum_package.SampleFunc;
import com.calabar.query.IHistoryQuery;
import com.calabar.query.IRealTimeQuery;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;


/**
 * <p/>
 * <li>Description: 请求控制器</li>
 * <li>@author: zijian.wu</li>
 * <li>Date: 2018/4/11 16:08</li>
 */
@RestController
public class QueryController {
    /**
     * 日志记录
     */
    private static final Logger LOGGER = LogManager.getLogger(QueryController.class);
    /**
     * 历史查询接口对象
     */
    @Autowired
    private IHistoryQuery historyQuery;
    /**
     * 实时查询接口对象
     */
    @Autowired
    private IRealTimeQuery realTimeQuery;

    /**
     * 实时查询控制方法GET
     *
     * @param json 查询条件json字符串
     * @return 结果json字符串
     * @throws Exception 参数异常
     */
    @RequestMapping(value = "/api/query/realtime", method = RequestMethod.GET, consumes = "application/json")
    String realtimeQueryGetAction(@RequestBody String json) throws Exception {

        //起始时间
        long queryStartTime=System.currentTimeMillis();

        //参数验证
        QueryParamsValidate.realtimeParamsValidate(json);

        LOGGER.info("query json :" + json);

        //转为json对象
        RealTimeQueriesEntity realTimeQueriesEntity = JSON.parseObject(json, RealTimeQueriesEntity.class);

        //获取属性
        List<String> stdCodes = realTimeQueriesEntity.getStdCodes();
        String pltCode = realTimeQueriesEntity.getPltCode();
        String setCode = realTimeQueriesEntity.getSetCode();

        //查询返回结果
        String results = realTimeQuery.realTimeQuery(stdCodes, pltCode, setCode);

        LOGGER.info("realtime query in "+(System.currentTimeMillis()-queryStartTime)+" ms");

        return results;
    }

    /**
     * 实时查询控制方法POST
     *
     * @param json 查询条件json字符串
     * @return 结果json字符串
     * @throws Exception 参数异常
     */
    @RequestMapping(value = "/api/query/realtime", method = RequestMethod.POST, consumes = "application/json")
    String realtimeQueryPostAction(@RequestBody String json) throws Exception {
        return realtimeQueryGetAction(json);
    }

    /**
     * 历史查询控制方法GET
     *
     * @param json 查询条件json字符串
     * @return 结果json字符串
     * @throws Exception 参数异常
     */
    @RequestMapping(value = "/api/query/history", method = RequestMethod.GET, consumes = "application/json")
    String HistoryQueryGetAction(@RequestBody String json) throws Exception {

        //起始时间
        long queryStartTime=System.currentTimeMillis();

        //参数验证
        QueryParamsValidate.historyQueriesValidate(json);

        LOGGER.info("query json :" + json);

        //转为json对象
        HistoryQueriesEntity historyQueriesEntity = JSON.parseObject(json, HistoryQueriesEntity.class);

        //获取属性
        List<String> stdCodes = historyQueriesEntity.getStdCodes();
        String pltCode = historyQueriesEntity.getPltCode();
        String setCode = historyQueriesEntity.getSetCode();
        Long startTime = historyQueriesEntity.getStartTime();
        Long endTime = historyQueriesEntity.getEndTime();
        Integer sampleInterval = historyQueriesEntity.getSampleInterval();
        Integer filling = historyQueriesEntity.getFilling();
        Integer sampleFunc = historyQueriesEntity.getSampleFunc();
        Filling fillingEnum = Filling.values()[filling];
        SampleFunc sampleFuncEnum = SampleFunc.values()[sampleFunc];

        //查询返回结果
        String results = historyQuery.historyQuery(stdCodes, pltCode, setCode, startTime, endTime, fillingEnum, sampleInterval, sampleFuncEnum);

        LOGGER.info("history query in "+(System.currentTimeMillis()-queryStartTime)+" ms");

        return results;
    }

    /**
     * 历史查询控制方法POST
     *
     * @param json 查询条件json字符串
     * @return 结果json字符串
     * @throws Exception 参数异常
     */
    @RequestMapping(value = "/api/query/history", method = RequestMethod.POST, consumes = "application/json")
    String HistoryQueryPostAction(@RequestBody String json) throws Exception {
        return HistoryQueryGetAction(json);

    }
}
