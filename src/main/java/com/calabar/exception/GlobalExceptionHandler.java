package com.calabar.exception;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * <p/>
 * <li>Description: 通用异常处理</li>
 * <li>@author: zijian.wu</li>
 * <li>Date: 2018/4/17 13:48</li>
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    /**
     * 日志记录
     */
    private static final Logger LOGGER = LogManager.getLogger(GlobalExceptionHandler.class);

    /**
     * 异常处理方法
     *
     * @param e 异常
     * @return 异常信息
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Map<String, Object> ExceptionHandler(Exception e) {
        //记录异常
        LOGGER.error(e.toString());

        Map<String, Object> response = new HashMap<>();
        response.put("error", "Request Error");
        response.put("massage", e.getMessage());
        response.put("status", 400);

        return response;
    }
}
