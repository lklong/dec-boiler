package com.calabar.exception;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * <p/>
 * <li>Description: 请求找不到路径处理</li>
 * <li>@author: zijian.wu</li>
 * <li>Date: 2018/4/17 14:40</li>
 */
@RestController
public class NotFoundHandler implements ErrorController {
    /**
     * 日志记录
     */
    private static final Logger LOGGER = LogManager.getLogger(NotFoundHandler.class);

    /**
     * 重写ErrorController方法
     *
     * @return 错误请求路径
     */
    @Override
    public String getErrorPath() {
        return "/error";
    }

    /**
     * 处理请求找不到路径方法
     *
     * @return 错误信息
     */
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @RequestMapping(value = "/error")
    public Map<String, Object> error() {
        //记录异常
        LOGGER.error("Endpoint not found");
        Map<String, Object> response = new HashMap<>();
        response.put("error", "Endpoint Not Found");
        response.put("status", 404);
        return response;
    }
}
