package com.calabar.query;


import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * <p/>
 * <li>Description: 东锅实时查询接口</li>
 * <li>@author: zijian.wu</li>
 * <li>Date: 2018/4/8 9:08</li>
 */
@Repository
public interface IRealTimeQuery {
    /**
     * @param stdCodes 测点标准编码
     * @param pltCode  测点对应的电厂编码
     * @param setCode  测点对应的机组编码
     * @return Json字符串结果
     * @throws Exception 参数异常，请求异常
     */
    public String realTimeQuery(List<String> stdCodes, String pltCode, String setCode) throws Exception;
}

