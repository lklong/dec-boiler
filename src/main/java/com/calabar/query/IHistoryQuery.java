package com.calabar.query;

import com.calabar.enum_package.Filling;
import com.calabar.enum_package.SampleFunc;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p/>
 * <li>Description: 东锅历史查询接口</li>
 * <li>@author: zijian.wu</li>
 * <li>Date: 2018/4/8 9:08</li>
 */
@Repository
public interface IHistoryQuery {
    /**
     * @param stdCodes       测点标准编码
     * @param pltCode        测点对应的电厂编码
     * @param setCode        测点对应的机组编码
     * @param startTime      起始时间
     * @param endTime        结束时间
     * @param filling        插值方式（不做插值，线性，取前）
     * @param sampleInterval 采样间隔（单位：秒）
     * @param func           采样函数（AVG,MIN, MAX,OPEN,CLOSE;）
     * @return Json字符串结果
     * @throws Exception 参数异常，请求异常
     */
    public String historyQuery(List<String> stdCodes, String pltCode, String setCode, long startTime,
                               long endTime, Filling filling, int sampleInterval, SampleFunc func) throws Exception;
}


