package com.calabar.query;


import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * <p/>
 * <li>Description: JDBC查询子系统编码</li>
 * <li>@author: zijian.wu</li>
 * <li>Date: 2018/4/19 9:30</li>
 */
@Repository
public class QuerySubStdCode {
    /**
     * 日志记录
     */
    private static final Logger LOGGER = LogManager.getLogger(QuerySubStdCode.class);
    /**
     * Spring封装的jdbcTemplate
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * kks编码表名
     */
    @Value("${query.mysql.kks-table-name}")
    private String kksTable;

    /**
     * 子系统编码字段名
     */
    @Value("${query.mysql.substdcode-name}")
    private String subStdCode;

    /**
     * 查询子系统编码与标准编码映射
     *
     * @param stdCodes 测点标准编码
     * @return 子系统编码与标准编码映射
     * @throws Exception 无此测点异常
     */
    public Map<String, String> findSubMapStd(List<String> stdCodes) throws Exception {
        Map<String, String> subMapStd = new LinkedHashMap<>();
        String stdCodeWithQuot = "";
        try {
            for (String stdCode : stdCodes) {
                //加引号
                if (!stdCode.contains("\'")) {
                    stdCodeWithQuot = "\'" + stdCode + "\'";
                } else {
                    stdCodeWithQuot = stdCode;
                }
                String sql = "select " + subStdCode + " from " + kksTable + " where is_valid=\'Y\'and kks_code=" + stdCodeWithQuot;
                String subStdCode = jdbcTemplate.queryForObject(sql, String.class);
                if (StringUtils.isNotEmpty(subStdCode)) {
                    subMapStd.put(subStdCode, stdCode);
                } else {
                    LOGGER.error("The stdCode " + stdCodeWithQuot + "does not exist");
                    throw new Exception("The stdCode " + stdCodeWithQuot + "does not exist");
                }
            }
        } catch (EmptyResultDataAccessException e) {
            LOGGER.error("The stdCode " + stdCodeWithQuot + "does not exist");
            throw new Exception("The stdCode " + stdCodeWithQuot + "does not exist");
        }
        return subMapStd;
    }

    /**
     * 查询标准编码与子系统编码映射
     *
     * @param stdCodes 测点标准编码
     * @return 标准编码与子系统编码映射
     * @throws Exception 无此测点异常
     */
    public Map<String, String> findStdMapSub(List<String> stdCodes) throws Exception {
        Map<String, String> stdMapSub = new LinkedHashMap<>();
        Map<String, String> subMapStd = findSubMapStd(stdCodes);
        //交换key与value
        subMapStd.forEach((key, value) -> stdMapSub.put(value, key));
        return stdMapSub;
    }
}
