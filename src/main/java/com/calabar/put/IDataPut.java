package com.calabar.put;

import com.calabar.entity.DataPutEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p/>
 * <li>Description: 数据写入接口</li>
 * <li>@author: zijian.wu</li>
 * <li>Date: 2018/4/20 13:41</li>
 */

@Repository

public interface IDataPut {
    /**
     *
     * @param data 数据
     * @throws Exception
     */
    public void put(List<DataPutEntity> data) throws Exception;
}
